//create database sistemas
create table estudiante
(
    //(id,nombre,ci,telf)
    id int not null primary key,
    nombre varchar(50) not null,
    ci  varchar(8) not null,
    telf varchar(10) not null
);

insert into estudiante values(1,'Pepe Perez','123456','147852');
insert into estudiante values(2,'Carlos Perez','654321','25847');
insert into estudiante values(3,'Pacho','963258','74125');