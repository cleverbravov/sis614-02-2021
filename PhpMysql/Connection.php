<?php
/*
Escribir un script para crear una base de datos: 
Sistemas
crear tabla estudiante(id,nombre,ci,telf)
mysql,postgres,sqlserver,mongodb

Escribir un programa para conectarse a la base de datos Sistemas
realizar un select y mostrar el resultado.

java,c++,c,*php,javascript,C#,python,etc.

*/
echo "begin.";
$host=getenv("MYSQL_HOST");
$username=getenv("MYSQL_USERNAME");
$password=getenv("MYSQL_PASSWORD");
$db=getenv("MYSQL_DB");
echo "trying to connect.";
$connection=mysqli_connect($host,$username,$password,$db);
if($connection->connect_errno)
{
    die('Error connecting to database.'.mysql_error());
}
else
{
    echo "Success!";
    $command="select * from estudiante";
    $result=$connection->query($command);
    while($r=$result->fetch_assoc())
    {
        echo "<br/>Id: ".$r["id"];
        echo "<br/>Nombre: ".$r["nombre"];
        echo "<br/>CI: ".$r["ci"];
        echo "<br/>Telf: ".$r["telf"];
        echo "<br/><br/>";
    }
}

?>